<?php
	require_once 'common.php';

    session_start();

	if(! isset($_SESSION['username']) || $_SESSION['username'] == '') { ?>
		Please login to translate, <a href="login.php">Login</a>
		<?php
		die();
    }
    
    function get_translation($word, $translations) {
        foreach($translations as $translation) {
            if($translation['tkey'] == $word) {
                return $translation['tvalue'];
            }
        }

        return $word;
    }

	function translate($file) {
		global $wd, $test_file, $pdo;
		try {
			$search_text = '';
			if($file) {
                $search_text = file_get_contents("$wd/$test_file");
			} else {
				$search_text = $_POST['search_text'];
			}
			$words = explode(' ', $search_text);
			$sql = "SELECT tkey, tvalue FROM translations WHERE username=:username";
			$stmt = $pdo->prepare($sql);
			$result = $stmt->execute(array('username' => $_SESSION['username']));
            $rows = $stmt->fetchAll();
            $translated_words = [];
			foreach($words as $word) {
                $translated_words[] = get_translation($word, $rows);
            }
            echo "Orignal: ".$search_text.'<br>';
            echo "Translation: ".implode(' ', $translated_words).'<hr>';
		} catch (Exception $e) {
			die("Could not translate");
		}
	}
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        if ($_FILES) {
            handle_file_upload($test_file);
            translate(true);
        } else {
            translate(false);
        }
    }

?>

<html>
<head>
</head>
<body>
Translate
<form method="POST" action="">
<textarea rows="5" cols="20" name="search_text"></textarea>
<input type="submit" value="Translate">
</form>
<hr>
Translate using files
<form method="POST" action="" enctype="multipart/form-data">
Select File: <input type="file" name="filename" size="10">
<input type="submit" value="Translate">
</form>
</body>
</html>
